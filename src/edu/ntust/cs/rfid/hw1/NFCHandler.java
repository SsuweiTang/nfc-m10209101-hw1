package edu.ntust.cs.rfid.hw1;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public interface NFCHandler {
public  boolean isSupported();
	
	public boolean isEnable();
	
	public void disableForeground();
	
	public void enableForegrount();

}
