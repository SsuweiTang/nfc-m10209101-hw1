**作業一NFC** 

設計一個讀取模式的NFC APP

具備以下兩大部分功能

Reading Tag 
▪ 可讀出 Tag 的類型與最大儲存容量 (Bytes)。
▪ 可讀出 Tag 的儲存內容 (TNF_WELL_KNOWN with RTD_URI)。
▪ 當讀出 URI 格式為 Tel (e.g. tel:xxxxxxxxxx) 時，可立即撥打電話。

 Writing Tag 
▪ 可建立 TNF_WELL_KNOWN with RTD_URI (tel) 型態的 Record。
▪ 將建立的 Record 包裝為 NDEF Message 然後寫入 Tag。