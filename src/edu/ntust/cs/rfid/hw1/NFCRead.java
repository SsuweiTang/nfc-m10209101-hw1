package edu.ntust.cs.rfid.hw1;

import java.nio.charset.Charset;
import java.util.Arrays;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.primitives.Bytes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class NFCRead extends Activity{
	
	private NFCHandler nfcHandler;
	private TextView tv_type=null;
	private TextView tv_size=null;
	private TextView tv_content=null;
	private Button btn_scanTag=null;
	IntentFilter [] gWriteTagFilters=null;
	private boolean tagCall=false;
	
	private String NDEFType=null;
	private int NDEFSize=0;
	private String MsgType=null;
	private String Payload=null;
	private String MsgId=null;
	private int MsgSize=0;
	private short MsgTnf=0;
	private String MessageContent=null;	
	private AlertDialog alertDialog = null;
	private final static String CALL = "android.intent.action.CALL";
	private Button btn_tab_read=null;
	private Button btn_tab_write=null;
	
	private static final BiMap<Byte, String> URI_PREFIX_MAP = ImmutableBiMap.<Byte, String>builder()
            .put((byte) 0x00, "")
            .put((byte) 0x01, "http://www.")
            .put((byte) 0x02, "https://www.")
            .put((byte) 0x03, "http://")
            .put((byte) 0x04, "https://")
            .put((byte) 0x05, "tel:")
            .put((byte) 0x06, "mailto:")
            .put((byte) 0x07, "ftp://anonymous:anonymous@")
            .put((byte) 0x08, "ftp://ftp.")
            .put((byte) 0x09, "ftps://")
            .put((byte) 0x0A, "sftp://")
            .put((byte) 0x0B, "smb://")
            .put((byte) 0x0C, "nfs://")
            .put((byte) 0x0D, "ftp://")
            .put((byte) 0x0E, "dav://")
            .put((byte) 0x0F, "news:")
            .put((byte) 0x10, "telnet://")
            .put((byte) 0x11, "imap:")
            .put((byte) 0x12, "rtsp://")
            .put((byte) 0x13, "urn:")
            .put((byte) 0x14, "pop:")
            .put((byte) 0x15, "sip:")
            .put((byte) 0x16, "sips:")
            .put((byte) 0x17, "tftp:")
            .put((byte) 0x18, "btspp://")
            .put((byte) 0x19, "btl2cap://")
            .put((byte) 0x1A, "btgoep://")
            .put((byte) 0x1B, "tcpobex://")
            .put((byte) 0x1C, "irdaobex://")
            .put((byte) 0x1D, "file://")
            .put((byte) 0x1E, "urn:epc:id:")
            .put((byte) 0x1F, "urn:epc:tag:")
            .put((byte) 0x20, "urn:epc:pat:")
            .put((byte) 0x21, "urn:epc:raw:")
            .put((byte) 0x22, "urn:epc:")
            .put((byte) 0x23, "urn:nfc:")
            .build();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_read);
		nfcHandler=new NFCHandlerImpl(this);
		findView();
		
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		nfcHandler.enableForegrount();
		 
		System.out.println("NFCRead=============Resume============="+getIntent().getAction()+tagCall);
		 if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
//			 Tag detectTag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
			 parserIntent(getIntent());
			 
	        }
		 if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())) {
//			 Tag detectTag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		
			 
	        }

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		 tagCall = false;
		nfcHandler.disableForeground();
		if (alertDialog != null)
            alertDialog.cancel();
		
		 
	}

	private void findView() {
		// TODO Auto-generated method stub
		tv_size = (TextView) findViewById(R.id.nfc_read_tv_size);
		tv_type = (TextView) findViewById(R.id.nfc_read_tv_type);
		tv_content = (TextView) findViewById(R.id.nfc_read_tv_content);
		btn_scanTag = (Button) findViewById(R.id.nfc_read_btn_scanTag);
		btn_tab_read=(Button)findViewById(R.id.tab_btn_nfc_read);
		btn_tab_write=(Button)findViewById(R.id.tab_btn_nfc_write);
		
		btn_tab_read.setOnClickListener(tabListener);
		btn_tab_write.setOnClickListener(tabListener);
		
//		btn_tab_read.setTag("tab_read");
//		btn_tab_write.setTag("tab_write");
		
		btn_scanTag.setOnClickListener(sanTagListener);
		
		if (!nfcHandler.isSupported()) {
//			tv_result.setText(R.string.nfc_unsupported);
			return;
		}

		if (!nfcHandler.isEnable()) {
//			tv_result.setText(R.string.nfc_disable);
		}
//		tv_result.setText(R.string.nfc_ready);
		
		
	}
	 @Override
	    public void onNewIntent(Intent intent) {
	        setIntent(intent);
	        tagCall = true;
	        System.out.println("====onNewIntent=="+intent.getAction());
	 }
	 
	 
	 
	private void parserIntent(Intent intent) {
		// TODO Auto-generated method stub

		System.out.println("==============ParserIntent===================");
		NdefMessage[] msgs = null;
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

		android.nfc.tech.Ndef getNdef = android.nfc.tech.Ndef.get(tag);
		
		NDEFType=getNdefType(getNdef.getType());
		NDEFSize = getNdef.getMaxSize();
		tv_type.setText(NDEFType);
		tv_size.setText(NDEFSize+"Byte");

		Parcelable[] rawMsgs = intent
				.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

		if (rawMsgs != null) {
			msgs = new NdefMessage[rawMsgs.length];
			for (int i = 0; i < rawMsgs.length; i++) {
				msgs[i] = (NdefMessage) rawMsgs[i];

				MsgSize += msgs[i].toByteArray().length;
				System.out.println("msg[" + i + "]" + msgs[i]);
			}
		} else {
			// Unknown tag type
			byte[] empty = new byte[] {};
			NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty,
					empty, empty);
			NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
			msgs = new NdefMessage[] { msg };

		}

		processMsg(msgs);

	}
	 
	 
	private String getNdefType(String type) {
		// TODO Auto-generated method stub
	 String myNdefType="";	
		
		if(type.toString().equals("com.nxp.ndef.mifareclassic"))
		{
			myNdefType="NDEF on MIFARE Classic";
		}
		else if(type.toString().equals("org.nfcforum.ndef.type1"))
		{
			myNdefType="NFC Forum Tag Type 1";
		}
		else if(type.toString().equals("org.nfcforum.ndef.type2"))
		{	
			myNdefType="NFC Forum Tag Type 2";
			
		}
		else if(type.toString().equals("org.nfcforum.ndef.type3"))
		{
			myNdefType="NFC Forum Tag Type 3";
		}
		else if(type.toString().equals("org.nfcforum.ndef.type4"))
		{
			myNdefType="NFC Forum Tag Type 4";
		}
		
			
    
		
		return myNdefType;
	}

	private void processMsg(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
		if (msgs == null || msgs.length == 0) {
			System.out.println("NdefMag==null");
		} else {
			for (int i = 0; i < msgs.length; i++) {
				int msglen = msgs[i].getRecords().length;
				NdefRecord[] records = msgs[i].getRecords();
				for (int j = 0; j < msglen; j++) // �X�ӰO��
				{
					for (NdefRecord record : records) {
						if (record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							if (Arrays.equals(record.getType(),
									NdefRecord.RTD_URI)) {
								Payload = parserWellKnownRtdUriPayload(record);
							}
						}
						MsgTnf = record.getTnf();
						MsgType = new String(record.getType());
						MsgId = new String(record.getId());
						MessageContent ="TNF==" + MsgTnf + "\n\n" + "MsgType=="
								+ MsgType + "\n\n" + "Size==" + MsgSize + "\n\n" + "Payload=="
								+ Payload;
						tv_content.setText(MessageContent);
						
						
						if (tagCall == true) {
							call(Payload);
						}

					}
				}

			}
		}

	}
	
	private void call(String myPayload) {
		// TODO Auto-generated method stub
		
		if(myPayload.startsWith("tel:",0))
		{
			Intent intentcall = new Intent(CALL, Uri.parse(myPayload));
	        startActivity(intentcall);
	       
	       
	        System.out.println("======call======="+tagCall);
		}
		 
	}

	private String parserWellKnownRtdUriPayload(NdefRecord record) {
		// TODO Auto-generated method stub
		
		Preconditions.checkArgument(Arrays.equals(record.getType(),
				NdefRecord.RTD_URI));
		byte[] payload = record.getPayload();
		String prefix = URI_PREFIX_MAP.get(payload[0]);
		byte[] fullUri = Bytes.concat(
				prefix.getBytes(Charset.forName("UTF-8")),
				Arrays.copyOfRange(payload, 1, payload.length));
		Uri uri = Uri.parse(new String(fullUri, Charset.forName("UTF-8")));

		return uri.toString();
	 	
	}
	
	
	private Button.OnClickListener sanTagListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			nfcHandler.enableForegrount();	
			
			 AlertDialog.Builder builder = new AlertDialog.Builder(NFCRead.this);
			 builder.setTitle("Approach a NFC Tag").setOnCancelListener(
                     new DialogInterface.OnCancelListener() {
                         @Override
                         public void onCancel(DialogInterface dialog) {
                        	 nfcHandler.disableForeground();
                         }
                     });
             alertDialog = builder.create();
             alertDialog.setCanceledOnTouchOutside(false);
             alertDialog.show();

		}
	};
	
	
	private Button.OnClickListener tabListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
		
			Intent intent = new Intent();
			intent.setClass(NFCRead.this, NFCWrite.class);
			startActivity(intent);
			finish();
			
			
			

		}
	};

}
