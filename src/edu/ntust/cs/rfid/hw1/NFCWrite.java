package edu.ntust.cs.rfid.hw1;

import java.io.IOException;
import java.nio.charset.Charset;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class NFCWrite extends Activity {

	private NFCHandler nfcHandler;
	private TextView tv_result = null;
	private Button btn_scanTag = null;
	IntentFilter[] gWriteTagFilters = null;
	private EditText edt_tel = null;
	private String data = "";
	private NdefMessage NDEFMsg = null;
	private AlertDialog alertDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_write);
		nfcHandler = new NFCHandlerImpl(this);
		findView();

	}

	@Override
	public void onNewIntent(Intent intent) {
		setIntent(intent);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		nfcHandler.enableForegrount();

		System.out.println("=============Resume============="
				+ getIntent().getAction());
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
			Tag detectTag = getIntent()
					.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) 0x05, false);
			System.out.println("=============" + data + "=============");
			writeTag(NDEFMsg, detectTag);
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		System.out.println("=============onPause============="
				+ getIntent().getAction());
		nfcHandler.disableForeground();

		if (alertDialog != null)
			alertDialog.cancel();
	}

	private void findView() {
		// TODO Auto-generated method stub

		tv_result = (TextView) findViewById(R.id.nfc_write_tv_result);
		btn_scanTag = (Button) findViewById(R.id.nfc_write_btn_ScanTag);
		edt_tel = (EditText) findViewById(R.id.nfc_write_edt_tel);
		btn_scanTag.setOnClickListener(sanTagListener);
		if (!nfcHandler.isSupported()) {
			tv_result.setText(R.string.nfc_unsupported);
			return;
		}

		if (!nfcHandler.isEnable()) {
			tv_result.setText(R.string.nfc_disable);
		}
		tv_result.setText(R.string.nfc_ready);

	}

	private Button.OnClickListener sanTagListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			data = edt_tel.getText().toString();

			nfcHandler.enableForegrount();
			AlertDialog.Builder builder = new AlertDialog.Builder(NFCWrite.this);
			builder.setTitle("Approach a NFC Tag").setOnCancelListener(
					new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							nfcHandler.disableForeground();
						}
					});
			alertDialog = builder.create();
			alertDialog.setCanceledOnTouchOutside(false);
			alertDialog.show();

		}
	};

	public static NdefMessage getNdefMsg_from_RTD_URI(String uriFiledStr,
			byte identifierCode, boolean flagAddAAR) {

		byte[] uriField = uriFiledStr.getBytes(Charset.forName("US-ASCII"));
		byte[] payLoad = new byte[uriField.length + 1]; // add 1 for the URI
		payLoad[0] = identifierCode; 
		System.arraycopy(uriField, 0, payLoad, 1, uriField.length);
		NdefRecord rtdUriRecord1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
				NdefRecord.RTD_URI, new byte[0], payLoad);
		return new NdefMessage(new NdefRecord[] { rtdUriRecord1 });

	}

	boolean writeTag(NdefMessage message, Tag tag) {
		int size = message.toByteArray().length;
		try {
			Ndef ndef = Ndef.get(tag);
			if (ndef != null) {
				ndef.connect();
				if (!ndef.isWritable()) {
					Toast.makeText(NFCWrite.this, "Tag is read-only.",
							Toast.LENGTH_SHORT).show();

					return false;
				}
				if (ndef.getMaxSize() < size) {
					Toast.makeText(
							NFCWrite.this,
							"The data cannot written to tag,Tag capacity is "
									+ ndef.getMaxSize() + " bytes, message is "
									+ size + " bytes.", Toast.LENGTH_SHORT)
							.show();
					return false;
				}
				ndef.writeNdefMessage(message);
				Toast.makeText(NFCWrite.this,
						"Message is written tag, message=" + message,
						Toast.LENGTH_SHORT).show();
				ndef.close();
				return true;
			} else {
				NdefFormatable format = NdefFormatable.get(tag);
				if (format != null) {
					try {
						format.connect();
						format.format(message);
						Toast.makeText(NFCWrite.this,
								"The data is written to the tag ",
								Toast.LENGTH_SHORT).show();
						return true;
					} catch (IOException e) {
						Toast.makeText(NFCWrite.this, "Failed to format tag.",
								Toast.LENGTH_SHORT).show();
						return false;
					}
				} else {
					Toast.makeText(NFCWrite.this, "Tag doesn't support NDEF.",
							Toast.LENGTH_SHORT).show();
					return false;
				}
			}
		} catch (Exception e) {
			Toast.makeText(NFCWrite.this, "Failed to write tag",
					Toast.LENGTH_SHORT).show();

		}
		return false;
	}

	private Button.OnClickListener tabListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getTag().equals("tab_write")) {
				Intent intent = new Intent();
				intent.setClass(NFCWrite.this, NFCRead.class);
				startActivity(intent);
				finish();
			}
		}
	};

}
