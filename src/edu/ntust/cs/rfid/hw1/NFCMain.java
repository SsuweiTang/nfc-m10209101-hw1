package edu.ntust.cs.rfid.hw1;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.primitives.Bytes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class NFCMain extends Activity {

	private NFCHandler nfcHandler;
	private TextView tv_type;
	private TextView tv_size;
	private TextView tv_content;
	private Button btn_scanTag;
	IntentFilter[] gWriteTagFilters;
	private boolean tagCall = false;
	private String NDEFType;
	private int NDEFSize = 0;
	private String MsgType;
	private String Payload;
	private String MsgId;
	private int MsgSize = 0;
	private short MsgTnf = 0;
	private String MessageContent;
	private AlertDialog alertDialog;
	private final static String CALL = "android.intent.action.CALL";
	private Button btn_tab_read ;
	private Button btn_tab_write;
	private LinearLayout li_read;
	private LinearLayout li_write;
	private boolean tag_write = false;
	private boolean tag_scan = false;
	private boolean tag_onResume = false;
	private String NdefTNF = "";
	private String writeResult = "";

	private static final BiMap<Byte, String> URI_PREFIX_MAP = ImmutableBiMap
			.<Byte, String> builder().put((byte) 0x00, "")
			.put((byte) 0x01, "http://www.").put((byte) 0x02, "https://www.")
			.put((byte) 0x03, "http://").put((byte) 0x04, "https://")
			.put((byte) 0x05, "tel:").put((byte) 0x06, "mailto:")
			.put((byte) 0x07, "ftp://anonymous:anonymous@")
			.put((byte) 0x08, "ftp://ftp.").put((byte) 0x09, "ftps://")
			.put((byte) 0x0A, "sftp://").put((byte) 0x0B, "smb://")
			.put((byte) 0x0C, "nfs://").put((byte) 0x0D, "ftp://")
			.put((byte) 0x0E, "dav://").put((byte) 0x0F, "news:")
			.put((byte) 0x10, "telnet://").put((byte) 0x11, "imap:")
			.put((byte) 0x12, "rtsp://").put((byte) 0x13, "urn:")
			.put((byte) 0x14, "pop:").put((byte) 0x15, "sip:")
			.put((byte) 0x16, "sips:").put((byte) 0x17, "tftp:")
			.put((byte) 0x18, "btspp://").put((byte) 0x19, "btl2cap://")
			.put((byte) 0x1A, "btgoep://").put((byte) 0x1B, "tcpobex://")
			.put((byte) 0x1C, "irdaobex://").put((byte) 0x1D, "file://")
			.put((byte) 0x1E, "urn:epc:id:").put((byte) 0x1F, "urn:epc:tag:")
			.put((byte) 0x20, "urn:epc:pat:").put((byte) 0x21, "urn:epc:raw:")
			.put((byte) 0x22, "urn:epc:").put((byte) 0x23, "urn:nfc:").build();

	private TextView tv_result;
	private EditText edt_tel;
	private String data = "";
	private NdefMessage NDEFMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_main);
		nfcHandler = new NFCHandlerImpl(this);
		findView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("NFCRead=============Resume============="
				+ getIntent().getAction() + tagCall);
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
			Tag detectTag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
			if (tag_write == true) {
				NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) 0x05, false);
				writeTag(NDEFMsg, detectTag);
				tv_result.setText(writeResult);
			} else {
				parserIntent(getIntent());
				// setIntent(new Intent());
			}
		}
		if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())) {
		
		}
		nfcHandler.enableForegrount();
	}

	@Override
	protected void onPause() {
		super.onPause();
		tagCall = false;
		nfcHandler.disableForeground();
		if (alertDialog != null) {
			alertDialog.cancel();
			// tag_scan=false;
		}
	}

	private void findView() {
		tv_size = (TextView) findViewById(R.id.nfc_main_read_tv_size);
		tv_type = (TextView) findViewById(R.id.nfc_main_read_tv_type);
		tv_content = (TextView) findViewById(R.id.nfc_main_read_tv_content);
		btn_scanTag = (Button) findViewById(R.id.nfc_main_btn_scanTag);
		btn_tab_read = (Button) findViewById(R.id.tab_btn_nfc_read);
		btn_tab_write = (Button) findViewById(R.id.tab_btn_nfc_write);
		li_read = (LinearLayout) findViewById(R.id.nfc_main_li_read);
		li_write = (LinearLayout) findViewById(R.id.nfc_main_li_write);
		tv_result = (TextView) findViewById(R.id.nfc_main_write_tv_result);
		edt_tel = (EditText) findViewById(R.id.nfc_main_write_edt_tel);
		btn_tab_read.setOnClickListener(tabListener);
		btn_tab_write.setOnClickListener(tabListener);
		btn_tab_read.setTag("tab_read");
		btn_tab_write.setTag("tab_write");
		btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray2));
		btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray));
		btn_scanTag.setOnClickListener(sanTagListener);
		if (!nfcHandler.isSupported()) {
			// tv_result.setText(R.string.nfc_unsupported);
			return;
		}

		if (!nfcHandler.isEnable()) {
			// tv_result.setText(R.string.nfc_disable);
		}
		// tv_result.setText(R.string.nfc_ready);
	}

	@Override
	public void onNewIntent(Intent intent) {
		if (tag_scan == true) {
			tag_scan = false;
			Vibrator myVibrator = (Vibrator) getApplication().getSystemService(Service.VIBRATOR_SERVICE);
			myVibrator.vibrate(500);
			setIntent(intent);
		}
		tagCall = true;
		System.out.println(tag_scan + "====onNewIntent==" + intent.getAction());
	}

	private void parserIntent(Intent intent) {
		System.out.println("==============ParserIntent===================");
		NdefMessage[] msgs = null;
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		Ndef getNdef = Ndef.get(tag);
		NDEFType = getNdefType(getNdef.getType());
		NDEFSize = getNdef.getMaxSize();
		tv_type.setText(NDEFType);
		tv_size.setText(NDEFSize + "Byte");

		Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
		if (rawMsgs != null) {
			msgs = new NdefMessage[rawMsgs.length];
			for (int i = 0; i < rawMsgs.length; i++) {
				msgs[i] = (NdefMessage) rawMsgs[i];
				MsgSize += msgs[i].toByteArray().length;
				System.out.println("msg[" + i + "]" + msgs[i]);
			}
		} else {
			byte[] empty = new byte[] {};
			NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty,empty, empty);
			NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
			msgs = new NdefMessage[] { msg };
		}
		processMsg(msgs);
	}

	private String getNdefType(String type) {
		String myNdefType = "";

		if (type.toString().equals("com.nxp.ndef.mifareclassic")) {
			myNdefType = "NDEF on MIFARE Classic";
		} else if (type.toString().equals("org.nfcforum.ndef.type1")) {
			myNdefType = "NFC Forum Tag Type 1";
		} else if (type.toString().equals("org.nfcforum.ndef.type2")) {
			myNdefType = "NFC Forum Tag Type 2";

		} else if (type.toString().equals("org.nfcforum.ndef.type3")) {
			myNdefType = "NFC Forum Tag Type 3";
		} else if (type.toString().equals("org.nfcforum.ndef.type4")) {
			myNdefType = "NFC Forum Tag Type 4";
		}

		return myNdefType;
	}

	private void processMsg(NdefMessage[] msgs) {
		if (msgs == null || msgs.length == 0) {
			System.out.println("NdefMag==null");
		} else {
			for (int i = 0; i < msgs.length; i++) {
				int msglen = msgs[i].getRecords().length;
				NdefRecord[] records = msgs[i].getRecords();
				for (int j = 0; j < msglen; j++) // 幾個紀錄
				{
					for (NdefRecord record : records) {
						if (record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							if (Arrays.equals(record.getType(),	NdefRecord.RTD_URI)) {
								Payload = parserWellKnownRtdUriPayload(record);
							} else if (Arrays.equals(record.getType(),NdefRecord.RTD_TEXT)) {
								Payload = WellKnownRtdTextPayload(record);
							}
						}
						if (record.getTnf() == NdefRecord.TNF_ABSOLUTE_URI) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_MIME_MEDIA) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						NdefTNF = getNDEF(record.getTnf());
						MsgType = getRecordType(new String(record.getType()));
						MsgId = new String(record.getId());
						MessageContent =  NdefTNF + "\n\n" + MsgType+ "\n\n" + Payload;
						tv_content.setText(MessageContent);
						tag_scan = false;
						if (tagCall == true	&& record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							call(Payload);
						}
					}
				}
			}
		}
	}
	private String WellKnownRtdTextPayload(NdefRecord record) {
		String myPayload = "";
		Preconditions.checkArgument(Arrays.equals(record.getType(),	NdefRecord.RTD_TEXT));
		byte[] payload = record.getPayload();
		Byte FirstByte = record.getPayload()[0];
		String txtRndcoding = ((FirstByte & 0200) == 0) ? "UTF-8" : "UTF-16";
		int langCodeLen = FirstByte & 0077;
		String langCode = new String(payload, 1, langCodeLen,Charset.forName("UTF-8"));
		try {
			myPayload = new String(payload, langCodeLen + 1, payload.length	- langCodeLen - 1, txtRndcoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("txtRndcoding==" + txtRndcoding + "  "	+ "langCodeLen  " + langCodeLen);
		return myPayload;
	}

	private String parserABSOLUTE_URIPayload(NdefRecord record) {
		byte[] payload = record.getPayload();
		Uri uri = Uri.parse(new String(payload, Charset.forName("UTF-8")));
		return uri.toString();
	}

	private String getRecordType(String type) {
		String myRecordType = "";
		if (type.equals("T"))
			myRecordType = "TEXT";
		else if (type.equals("U"))
			myRecordType = "URI";
		else if (type.equals("Sp"))
			myRecordType = "SMART_POSTER";
		else if (type.equals("ac"))
			myRecordType = "ALTERNATIVE_CARRIER";
		else if (type.equals("Hc"))
			myRecordType = "HANDOVER_CARRIER";
		else if (type.equals("Hr"))
			myRecordType = "HANDOVER_REQUEST";
		else if (type.equals("Hs"))
			myRecordType = "HANDOVER_SELECT";
		else {
			myRecordType = "";
		}
		return myRecordType;
	}

	private String getNDEF(short tnf) {
		String myNdefTNF = "";
		if (tnf == NdefRecord.TNF_EMPTY) // 000
		{
			myNdefTNF = "TNF_EMPTY";
		}
		if (tnf == NdefRecord.TNF_WELL_KNOWN) // 001
		{
			myNdefTNF = "TNF_WELL_KNOWN";
		}
		if (tnf == NdefRecord.TNF_MIME_MEDIA)// 002
		{
			myNdefTNF = "TNF_MIME_MEDIA";
		}
		if (tnf == NdefRecord.TNF_ABSOLUTE_URI)// 003
		{
			myNdefTNF = "TNF_ABSOLUTE_URI";
		}
		if (tnf == NdefRecord.TNF_EXTERNAL_TYPE) // 004
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNKNOWN) // 005
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNCHANGED) // 006
		{
			myNdefTNF = "TNF_UNCHANGED";
		}
		return myNdefTNF;
	}

	private void call(String myPayload) {
		if (myPayload.startsWith("tel:", 0)) {
			Intent intentcall = new Intent(CALL, Uri.parse(myPayload));
			startActivity(intentcall);
			System.out.println("======call=======" + tagCall);
		}
	}
	private String parserWellKnownRtdUriPayload(NdefRecord record) {
		Preconditions.checkArgument(Arrays.equals(record.getType(),	NdefRecord.RTD_URI));
		byte[] payload = record.getPayload();
		String prefix = URI_PREFIX_MAP.get(payload[0]);
		byte[] fullUri = Bytes.concat(prefix.getBytes(Charset.forName("UTF-8")),Arrays.copyOfRange(payload, 1, payload.length));
		Uri uri = Uri.parse(new String(fullUri, Charset.forName("UTF-8")));
		return uri.toString();
	}
	private Button.OnClickListener sanTagListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			nfcHandler.enableForegrount();
			tag_scan = true;
			if (tag_write == true) {
				if(!edt_tel.getText().toString().equals(""))
				{
				   data = edt_tel.getText().toString();
				}
				else
				{
					Toast.makeText(NFCMain.this, "telephone is null", Toast.LENGTH_SHORT).show();
				}
			}
	
			AlertDialog.Builder builder = new AlertDialog.Builder(NFCMain.this);
			builder.setTitle("Approach a NFC Tag");
			// builder.setMessage("Approach a NFC Tag");
			builder.setIcon(getResources().getDrawable(R.drawable.dialogicon));
			builder.setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							nfcHandler.disableForeground();
							alertDialog.cancel();
						}
					});
			if (edt_tel.getText().toString().equals("") && tag_write == true) {
			} else {
				alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(false);
				alertDialog.show();
			}

		}
	};

	private Button.OnClickListener tabListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getTag().toString().equals("tab_read")) {
				tag_write = false;
				li_read.setVisibility(View.VISIBLE);
				li_write.setVisibility(View.GONE);
				btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray2));
				btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray));

			}
			if (v.getTag().toString().equals("tab_write")) {
				tag_write = true;
				li_read.setVisibility(View.GONE);
				li_write.setVisibility(View.VISIBLE);
				btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray2));
				btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray));
			}

		}
	};

	public static NdefMessage getNdefMsg_from_RTD_URI(String uriFiledStr,byte identifierCode, boolean flagAddAAR) {
		byte[] uriField = uriFiledStr.getBytes(Charset.forName("US-ASCII"));
		byte[] payLoad = new byte[uriField.length + 1]; 
		payLoad[0] = identifierCode; 
		System.arraycopy(uriField, 0, payLoad, 1, uriField.length);
		NdefRecord rtdUriRecord1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_URI, new byte[0], payLoad);
		return new NdefMessage(new NdefRecord[] { rtdUriRecord1 });
	}

	boolean writeTag(NdefMessage message, Tag tag) {
		int size = message.toByteArray().length;
		try {
			Ndef ndef = Ndef.get(tag);
			if (ndef != null) {
				ndef.connect();
				try {
					if (!ndef.isWritable()) {
						writeResult = "Tag is read-only.";
						return false;
					} else if (ndef.getMaxSize() < size) {
						writeResult = "The data cannot written to tag,Tag capacity is "
								+ ndef.getMaxSize()
								+ " bytes, message is "
								+ size + " bytes.";
						return false;
					} else {
						ndef.writeNdefMessage(message);
						System.out.println("message=" + message);
						writeResult = "Writte tag successful" + "\n\n";
						tag_scan = false;
						return true;
					}

				} catch (IOException e) {
					writeResult = "Tag refused to connect.";
				} finally {
					ndef.close();
				}

			} else {
				NdefFormatable format = NdefFormatable.get(tag);
				if (format != null) {
					try {
						format.connect();
						try {
							format.format(message);
							writeResult = "The data is written to the tag ";
							return true;
						} catch (IOException e) {
							writeResult = "Failed to format tag.";
						}
					} catch (IOException e) {
						writeResult = "Failed to connect tag.";
						return false;
					} finally {
						format.close();
					}
				} else {
					writeResult = "Tag doesn't support NDEF.";
					return false;
				}
			}
		} catch (Exception e) {
			writeResult = "Failed to write tag";
		}
		return false;
	}

}
